# Repository Portfolio

This is arepository pointing towards other repositories on GitHub and GitLab that contain code I have written/contributed to.

Most of these repositores are private for academic reasons, though access may be granted if requested.

You may direct any such requests to *carterboclair268@gmail.com* or *carter.boclair@utexas.edu* with the GitLab ID you wish to be added.

I have added the team planning/documentation for all major assignments to this repository as MD or PDF files.

Additionally, any video demonstrations of these assignments may be found in `VideoDemoLinks.md`

# Repository Links

Below are all existing repositores as of October 28, 2021. More may be added as I progress in the classes.

## CS378: Game Programming Paradigms Repositories

**Assignment 1: Physics-based Game** [Repository](https://gitlab.com/jbozon/cs378-a1)

**Assignment 2: Player Package** [Repository](https://gitlab.com/Carb-UT/cs378-a2)

**Assignment 3: AI & Networking** [Repository](https://gitlab.com/Carb-UT/cs378-a3) *In Progress*

## CS354R: Game Technology Repositories

**Assignment 1: Building in Game Engines** [Repository](https://gitlab.com/Carb-UT/cs354r-a1)

**Assignment 2: Physics and Player Package** [Repository](https://gitlab.com/nk_ultra/cs354r-assignment-2/-/tree/codefreeze)

**Assignment 3: Networked Play** [Repository](https://gitlab.com/nk_ultra/cs354r-assignment-2/-/tree/codefreeze-A3)

*Note: Assignment 2 and 3 are located on the same repository. Different branches contain each assignment.*

## Additional Repositories

**Texas 2021 Game Jam Submission Repository:** [Waking Dreams Repository](https://github.com/PurpleML/CovenJam)

# Additional Links

**Texas 2021 Game Jam Submission**: [Waking Dreams](https://purpleml.itch.io/waking-dreams/)

**LinkedIn Profile:** [LinkedIn](https://www.linkedin.com/in/carter-boclair-a36842224/)


