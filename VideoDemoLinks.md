# Links to video demonstrations of linked assignment repositories:

## CS378: Game Programming Paradigms Video Demonstrations

**Assignment 1: Physics-based Game** [Demonstration](https://youtu.be/gLPircDkiuk)

**Assignment 2: Player Package** [Demonstration](https://youtu.be/tycN4Q7133M)

**Assignment 3: AI & Networking** [Demonstration](https://youtu.be/O3-OzJs85bk) *In Progress*

**Assignment 4: Game State** *To Be Completed*

**Assignment 5: Your Own Game** *To Be Completed*

## CS354R: Game Technology Video Demonstrations

**Assignment 1: Buildings in Game Engines** [Demonstration](https://youtu.be/LdE1q0mPTG4)

**Assignment 2: Physics and Player Package** [Demonstration](https://youtu.be/jiWRpoOg7Q8)

**Assignment 3: Networked Play** [Demonstration](https://youtu.be/HoPzjA8fvJA)

**Assignment 4: AI & NPCS** *To Be Completed*

**Final Assignment: Your Own Game** *To Be Completed*

