# Team Steel 

## Overall Design

- We want to make something that isn't too big. We're thinking of making an exploration sandbox. There will be collectables and enemies.

- **Scoring**:
    - One point per collectable
    - Immediate death upon touching an enemy
- **Visuals**:
    - Simple geometric shapes
    - If time permits, use of more complex models
    - Theme: Island in the ocean
    - Player idea: Explorer looking for treasure
- **GUI**:
    - Score tracker: Objects collected/remaining
    - Restart button if player dies/loses
    - End screen after collecting all objects
    - Two main signals: Collect and Death
- **Sound**:
    - Walking sounds
    - Ambient sounds
    - Pickup jingle
    - Death sound
    - Sources: freesounds.org

    ---

## Software Architecture
- **Scenes**:
    - Player Controller
        - Emits signal upon interacting with object/enemy
        - Movement/constraints (ladder-climbing/ledge-hanging)
            - Moving: WASD
            - Jumping: Space
            - Climbing/ledge-hanging: <reserved-key>
    - Camera Controller
        - Rotation: Mouse movement
        - Zooming: Mouse wheel
        - Parameters based on axis movement
        - Limits set in editor
        - Spatial node serves as boom arm for camera
        - Dithering/Culling nearby objects
    - Audio
        - Child node of audio source
    - GUI
        - Score keeper
        - End screen
        - Restart button
        - Listen for player signals
    - Physics
        - Assumed to be wrapped in, so no standalone class
    - CollectableObject
        - Able to collide with player for pickup
        - Contains AudioStreamPlayer to play collecting-jingle
        Collision callback
    - Enemy
        - If player collides with this, end the game
        - Primitive AI (patrol AI, chase player)
        - Contains AudioStreamPlayer to play death noise
        - Collision callback

## Long-term plans
- Scaling back
    - Enemy might be overambitious to implement. We might replace it with another collectable object with a different sound effect.
    - Audio controller could be removed to save time.
    - GUI: Restart functionality can be replaced with a single button press

## Division of labor:
- Carter:
    - Camera Controller
    - Audio
- Joey:
    - PlayerController basics
- Hogan:
    - GUI
- Noah:
    - Minecraft-style flying
    - CollectableObject
    - Enemy
