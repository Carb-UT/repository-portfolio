# *Manual for Team Steal Collect-a-thon Game:*

## **Goal of the game:**

- The goal of our game is simply to find and collect the coins and avoid enemies.

- The counters along the top show the number of enemies and number of collectables still present in the scene.
    - Sword: Enemies
    - Shield: Coins

## **Controls**
- By default, the camera is moved with the mouse and character movement happens relative to camera position. 
- However, if you toggle the camera mode with T then you will enter "tank mode" where the camera follows player movement and A/D turn into rotation buttons while W/D move still move forwards and back.

- Movement is simple WASD movement with a jump bound to the Spacebar.

- To enter flying mode, press V while in the air. In this mode you move similarly to on the ground but may press Space to ascend and Left Control to descend.

- To climb ladders, approach the ladder and press Q to enter the alternate movement mode for ladders. In ladder mode you move similarly to flying, Space to climb up and Left Control to climb down. At the top of the ladder you will be allowed to move off or you can press Q to leave ladder mode.

- By default, the player will simply fall off of ledges. P will cycle between that, stopping at ledges, and hanging from ledges.

- To pick up interactables, simply press E and if you are standing over an interactable it will be collected and you will hear a jingle.

- If there is no interactable you will hear a different sound to indicate this.

- By default, the camera is moved with the mouse and character movement happens relative to camera position. 

- However, if you toggle the camera mode with T then you will enter "tank mode" where the camera follows player movement and A/D turn into rotation buttons while W/D move still move forwards and back.

## **Key Mappings:**

### Movement

| Action | Key |
| ---- | ---- |
| Forward | W |
| Backward | S |
| Left Strafe | A |
| Right Strafe | D |
| Jump | Space |
| Flight | V (while in the air) |

### Sound
| Action | Key |
| ---- | ---- |
| Toggle SFX | 4 |
| Toggle BG Music | 3 |
| Music Volume Up | 2 |
| Music Volume Down | 1 |


### Miscellaneous:
| Action | Key |
| ---- | ---- |
| Interact | E |
| Ladder Interact | Q |
| Change Camera Mode | T |
| Change Ledge Mode | P |



