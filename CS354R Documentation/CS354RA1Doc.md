_**CS354R Assignment 1 Project Report:**_


_**Functionality Implemented:**_
    
**Walls:** Walls possess areas and meshses and contain hard-coded normal vectors with an accessor function:

`private: Vector3 normal;`

`public: Vector3 get_normal() { return normal; };`


**Balls:** Balls possess areas and meshes, each ball is given a randomized velocity within `_start()`:

`int velx = std::rand() % 20 + 3;`

`int vely = std::rand() % 20 + 3;`

`int velz = std::rand() % 20 + 3;`

`velocity = Vector3(velx, vely, velz);`

The same process with different bounds is done to randomize ball starting positions. 
 
In `_process()` balls move relative to their velocity and the time delta:

`set_translation(get_translation() + delta * velocity);`
        
Balls can detect collisions with walls and other balls through a callback established in `_ready()`:

`area->connect("area_entered", this, "bounce");`

Multiple collisions are simply handled FIFO based on callback order. No special force transfer code is present.

In bounce balls calculate normals for collisions, reflect their velocity, and re-orient:

Ball Collisions:
`normal = (Object::cast_to<A1Ball>(parent)->get_translation() - get_translation()).normalized();`

Wall Collision:
`normal = Object::cast_to<A1Wall>(parent)->get_normal();`

Set velocity and Orientation:
`velocity = velocity - ((2 * (velocity.dot(normal))) * normal);`
`look_at(velocity, Vector3(0, 1, 0));`


_**Interesting Features:**_

There are not any particularly "interesting features" in this assignment currently. It is merely the base assignment.

_**External Software Used:**_

I programmed in Visual Studio 2019 and consulted the Godot documentation.
My textures are just some .png files I made in a drawing software.
Other than that, I did not use any external tools.

_**Issues Encountered:**_

Learning about the Godot build system was, by far, the hardest part of this assignment. 

Beyond that, simply figuring out how to properly get data about nodes and establishing the callback were the most complicated parts of code.

I did have a temporary bug where I used * instead of % when setting velocities, which broke the physics.

_**Unresolved Issues:**_

I am not presently aware of any unresolved issues regarding this project.

_**Program Use Instructions:**_

After building the project with SCons, simply open within the Godot Editor and hit play to watch the balls.

_**Video Link:**_

https://youtu.be/LdE1q0mPTG4





